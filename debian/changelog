gtkhash (1.5-3) unstable; urgency=medium

  * upload to sid
  * d/copyright: update-debian-copyright to 2025 year

 -- xiao sheng wen <atzlinux@sina.com>  Wed, 08 Jan 2025 10:55:18 +0800

gtkhash (1.5-3~exp1) experimental; urgency=medium

  * add d/p/fix-configure.ac-gettext-version.patch(Closes: #1092226)

 -- xiao sheng wen <atzlinux@sina.com>  Tue, 07 Jan 2025 08:58:19 +0800

gtkhash (1.5-2) unstable; urgency=medium

  * d/copyright: update year info to 2024
  * d/control: Bump Standards-Version: 4.7.0, no changes needed
  * add d/p/fix-msgfmt-keyword-use-space.patch (Closes: #1074731)

 -- xiao sheng wen <atzlinux@sina.com>  Fri, 05 Jul 2024 17:40:53 +0800

gtkhash (1.5-1) unstable; urgency=medium

  * Bump Standards-Version: 4.6.2
  * New upstream version 1.5
  * Run wrap-and-sort -a -t
  * Update-debian-copyright year to 2023

 -- xiao sheng wen <atzlinux@sina.com>  Fri, 03 Mar 2023 11:40:40 +0800

gtkhash (1.4+git20220617-3) unstable; urgency=medium

  [ Jesús Soto ]
  * remove nautilus extension
    for transition: nautilus 43, see #1016988.

 -- xiao sheng wen <atzlinux@sina.com>  Mon, 12 Sep 2022 18:16:50 +0800

gtkhash (1.4+git20220617-2) unstable; urgency=medium

  * d/clean: add config.log
  * d/control: Build-Depends: add librsvg2-common, fix dh_auto_test error

 -- xiao sheng wen <atzlinux@sina.com>  Tue, 16 Aug 2022 23:08:30 +0800

gtkhash (1.4+git20220617-1) unstable; urgency=medium

  * Set debhelper-compat version in Build-Depends.
  * Drop unnecessary dependency on dh-autoreconf.
  * debhelper-compat 13 (routine-update)
  * Reorder sequence of d/control fields by cme (routine-update)
  * Remove trailing whitespace in debian/copyright (routine-update)
  * Rules-Requires-Root: no (routine-update)
  * Bump Standards-Version: 4.6.1
  * update d/watch, use tags
  * New maintainer (Closes: #1015845)
  * d/copyright:
      - add src/hash/md6/* MIT License
      - update Upstream-contact email and year info
      - add Files: po/* infos
      - add 2022 xiao sheng wen <atzlinux@sina.com>
  * New upstream version 1.4+git20220617 (Closes: #978830)
  * remove d/patches, not need
  * update *.install, rename metainfo.xml files name, add org.gtkhash prefix
  * update d/gtkhash.install
     - rename desktop file to org.gtkhash.gtkhash.desktop
     - rename gtkhash.appdata.xml to org.gtkhash.gtkhash.appdata.xml
  * update d/caja-gtkhash.install
     - add libgtkhash-properties-caja.caja-extension file
  * add d/upstream/metadata
  * rm debian/source/local-options, don't use
  * add d/clean
  * d/control:
      - Build-Depends: + librsvg2-bin, - intltool
      - Build-Depends: + at-spi2-core, dh_auto_test need to use org.a11y.Bus
      - Update Homepage: https://gtkhash.org
  * d/rules: tune for new upstream version
      - add execute_before_dh_missing target
      - remove DEB_HOST_MULTIARCH variable, not use
      - delete override_dh_makeshlibs target
      - comment export DH_VERBOSE=1

 -- xiao sheng wen <atzlinux@sina.com>  Mon, 15 Aug 2022 16:58:57 +0800

gtkhash (1.2-1) unstable; urgency=medium

  * New upstream release.
  * Remove 20-fix-component-id.patch, 30-add-image-screenshot.patch,
    50-support-thunarx-3.patch: applied upstream.
  * Install files: upstream metainfo folder now is named appdata.
  * Bump to Standards-Version 4.2.1. No changes required.

 -- Mònica Ramírez Arceda <monica@debian.org>  Sat, 22 Dec 2018 11:09:50 +0100

gtkhash (1.1.1-3.1) unstable; urgency=medium

  * Non-maintainer upload.
  * Support thunarx-3 (Closes: #908609):
    + d/p/11-support-thunarx-3.patch: Add upstream commit to support thunarx-3.
    + d/control: Adjust build-dep: libthunarx-2-dev → libthunarx-3-dev.
    + d/thunar-gtkhash.install: Adapt to the thunarx-3 path.

 -- Unit 193 <unit193@ubuntu.com>  Mon, 24 Sep 2018 19:12:36 -0400

gtkhash (1.1.1-3) unstable; urgency=medium

  * Fix Vcs-*.
  * Fix AppStream errors and warnings.
    - 20-fix-component-id.patch: fix component IDs.
    - 30-add-image-screenshot.patch: Add image element to screenshot element.
    - 40-fix-invalid-tags.patch: Fix invalid tags.
  * Update debian/watch: use GitHub instead of SourceForge.
  * Bump to Standards-Version 4.1.5. No changes required.

 -- Mònica Ramírez Arceda <monica@debian.org>  Mon, 16 Jul 2018 13:46:10 +0200

gtkhash (1.1.1-2) unstable; urgency=medium

  * Remove gconf dependency. (Closes: #894654)
  * Remove dh-autoreconf dependency: debhelper enables the autoreconf sequence
    by default.
  * Update compatibility level to 11.
  * Use HTTPS in copyright format URL.
  * Bump to Standards-Version 4.1.4. No changes required.
  * Migrate Vcs-* to salsa.debian.org.

 -- Mònica Ramírez Arceda <monica@debian.org>  Tue, 10 Apr 2018 15:01:53 +0200

gtkhash (1.1.1-1) unstable; urgency=medium

  * New upstram release. (Closes: #884460)
  * Add libb2-dev dependency to support BLAKE2 family of hash functions.
  * Bump to Standards-Version 4.1.2. No changes required.

 -- Mònica Ramírez Arceda <monica@debian.org>  Fri, 22 Dec 2017 22:00:06 +0100

gtkhash (1.1-1) unstable; urgency=medium

  * New upstream release.
  * Remove 20-fix-xvfb.patch and 30-fix-random-test-failure.patch:
    applied upstream.
  * Bump to Standards-Version 4.1.0. No changes required.
  * Move metainfo files from /usr/share/appdata to /usr/share/metainfo.
  * Remove leading / from *.install files

 -- Mònica Ramírez Arceda <monica@debian.org>  Tue, 05 Sep 2017 17:38:27 +0200

gtkhash (1.0-2) unstable; urgency=medium

  * Fix random test failure from race condition. (Closes: #852893)

 -- Mònica Ramírez Arceda <monica@debian.org>  Mon, 06 Feb 2017 18:44:27 +0100

gtkhash (1.0-1) unstable; urgency=medium

  * New upstream release.
  * Update patches:
    - 10-avoid-silent-rules.patch: updated.
    - 20-remove-timestamp.patch: removed.
  * Add xvfb and xauth to Build-Depends, needed by gtkhash tests.
  * Set HOME for tests to a writeable directory.
  * 20-fix-xvfb.patch: add resolution to xvfb-run to make tests work fine,
    patch applied upstream.
  * Install files: adapt to new upstream names.
  * Add upstream desktop file.
  * Use libgcrypt and nettle instead of mhash.
  * Remove gtkhash-common binary package.
  * Add caja extension, a new binary package with name caja-gtkhash is
    provided.

 -- Mònica Ramírez Arceda <monica@debian.org>  Sat, 21 Jan 2017 16:16:59 +0100

gtkhash (0.7.0-4) unstable; urgency=medium

  * Install nautilus files. Nautilus has been multi-archified.
    Thanks to Jeremy Bicha! (Closes: #838743)

 -- Mònica Ramírez Arceda <monica@debian.org>  Tue, 04 Oct 2016 14:28:11 +0200

gtkhash (0.7.0-3) unstable; urgency=medium

  * Replace glib-gettext.m4 with a newer file to avoid FTBFS.
    (Closes: #819837)
  * Use https protocol for vcs-git and vcs-browser.
  * Remove repeated license in debian/copyright.
  * Remove Debian menu file, deprecated in favor of desktop file.
  * Upgrade to Standards-Version 3.9.8.
  * Add -n flag to gzip to avoid timestamps and make the package reproducible.

 -- Mònica Ramírez Arceda <monica@debian.org>  Fri, 29 Apr 2016 22:57:45 +0200

gtkhash (0.7.0-2) unstable; urgency=medium

  * Use dh-autoreconf instead of autotools-dev to also fix FTBFS on ppc64el by
    getting new libtool macros (still updates config.{sub,guess}). Thanks to
    Logan Rosen <logan@ubuntu.com>! (Closes: #744589)

 -- Mònica Ramírez Arceda <monica@debian.org>  Tue, 22 Apr 2014 20:55:04 +0200

gtkhash (0.7.0-1) unstable; urgency=low

  * New upstream release.
  * Add nemo extension, a new binary package with name nemo-gtkhash is
    provided:
    - debian/control: add nemo-gtkhassh paragraph and
                      libnemo-extension-dev dependency.
    - debian/rules: enable nemo extension.
    - debian/nemo-gtkhash.install: install needed file to provide nemo
      extension.
  * debian/control:
    - Bump to Standards-Version 3.9.5. No changes required.
    - Use canonical URIs in VCS-* fields.
  * debian/gtkhash.desktop: add Keywords key.
  * debian/patches/10-avoid-silent-rules.patch: don't hide the real compiler
    flags to allow automatic checks for missing (hardening) flags via blhc.
  * debian/rules: Update config.{guess,sub} files via autotools_dev.
    (Closes: #727889)

 -- Mònica Ramírez Arceda <monica@debian.org>  Tue, 26 Nov 2013 09:44:49 +0100

gtkhash (0.6.0-4) unstable; urgency=low

  * debian/compat: update to 9.
  * debian/control:
    - Update Maintainer field with Debian email.
    - Update to debhelper 9.
    - Bump to Standards-Version 3.9.3. No changes required.
  * Remove fix-mhash-segmentation-fault.patch: #653072 is fixed, therefore
    this patch is not needed anymore.
  * debian/rules: enable security hardening build flags.
  * debian/copyright: update Format field.

 -- Mònica Ramírez Arceda <monica@debian.org>  Sat, 23 Jun 2012 22:43:29 +0200

gtkhash (0.6.0-3) unstable; urgency=low

  * Force gtkhash-common to break with gtkhash (<< 0.6.0):
    they have files in common. (Closes: #656842)
  * Add Replaces: field in conjunction with Breaks:

 -- Mònica Ramírez Arceda <monica@probeta.net>  Mon, 23 Jan 2012 01:20:23 +0100

gtkhash (0.6.0-2) unstable; urgency=low

  * Force nautilus-gtkhash to break with gtkhash (<< 0.6.0):
    they have a file in common.
  * Move translations to gtkhash-common package and force gtkhash
    to depends on it.

 -- Mònica Ramírez Arceda <monica@probeta.net>  Sat, 21 Jan 2012 16:06:40 +0100

gtkhash (0.6.0-1) unstable; urgency=low

  * New upstream release.
  * Remove add.gtk3.support.patch: new upstream release supports GTK3.
  * Remove ignore.pc.translations.patch: as add.gtk3.support.patch is removed,
    this patch is no longer needed.
  * Add fix-mhash-segmentation-fault.patch to avoid mhash bug #653072.
  * debian/rules:
    - Remove dh_auto_clean override: it is no longer needed.
    - Enable the mhash library.
    - Add --list-missing option to dh_install.
  * Split the package in four binary packages to separate nautilus and thunar
    extensions from the standalone program:
    - debian/control: add libthunarx-2-dev dependency, add gtkhash-common,
      nautilus-gtkhash and thunar-gtkhash paragraphs.
    - debian/rules: enable nautilus and thunar extensions..
    - debian/gtkhash.install, debian/nautilus-gtkhash.install,
      debian/thunar-gtkhash.install: where to install each file.
  * debian/gtkhash.1: change reference to mhash manpage, it is in section 3
    instead of section 1.

 -- Mònica Ramírez Arceda <monica@probeta.net>  Wed, 21 Dec 2011 11:28:33 +0100

gtkhash (0.3.0-6) unstable; urgency=low

  * Team upload.
  * Upload to unstable.

 -- Michael Biebl <biebl@debian.org>  Fri, 14 Oct 2011 06:40:40 +0200

gtkhash (0.3.0-5) experimental; urgency=low

  * Update to gtk-3 and nautilus-3 (Closes: #637329).
    - debian/contol: update dependencies.
    - debian/rules: force to use GTK-3.
    - add.gtk3.support.patch: add needed changes to use GTK-3.
  * ignore.pc.translations.patch: Add POTFILES.skip file to avoid
    POTFILE error for files in .pc directory.
  * debian/rules: Make sure there will be no *.la files.

 -- Mònica Ramírez Arceda <monica@probeta.net>  Mon, 22 Aug 2011 16:16:59 +0200

gtkhash (0.3.0-4) unstable; urgency=low

  * Adopt the package. (Closes: #611693)
  * Switch to 3.0 (quilt) format.
  * Bump to Standards-Version 3.9.2. No changes required.
  * Add Debian menu item and reduce gtkhash.xpm to 32x32 pixels.
  * Update debian/copyright to DEP5 revision 174.

 -- Mònica Ramírez Arceda <monica@probeta.net>  Wed, 08 Jun 2011 21:07:57 +0200

gtkhash (0.3.0-3) unstable; urgency=low

  * QA upload.
  * Orphaning this.

 -- Alessio Treglia <alessio@debian.org>  Wed, 30 Mar 2011 12:23:59 +0200

gtkhash (0.3.0-2) unstable; urgency=low

  * debian/control:
    - Add Vcs-* tags.
    - Improve runtime description (Closes: #563486).
    - Adjust debhelper build-dependency to make backporters' life easy.
    - Set DM-Upload-Allowed to yes.

 -- Alessio Treglia <quadrispro@ubuntu.com>  Mon, 25 Jan 2010 18:49:07 +0100

gtkhash (0.3.0-1) unstable; urgency=low

  * Initial release (Closes: #560213).

 -- Alessio Treglia <quadrispro@ubuntu.com>  Wed, 09 Dec 2009 19:20:54 +0100
